from flask import Flask, json, make_response, jsonify, request
from flask_cors import CORS
from pymongo import MongoClient
from bson import ObjectId
import jwt
import bcrypt
import datetime
from functools import wraps

app = Flask(__name__)
CORS(app)

app.config['SECRET_KEY'] = 'mysecret'

client = MongoClient( "mongodb://localhost:27017" )
db = client['supermarket-db']
products = db.products
users = db.users

#**Decorators**
#JWT
def jwt_required(func):
  @wraps(func)
  def jwt_required_wrapper(*args, **kwargs):
    token = None
    if 'x-access-token' in request.headers:
      token = request.headers['x-access-token']
    if not token:
      return jsonify({'message' : 'Token is missing'}), 401
    try:
      data = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
      user = users.find_one({"username": data["username"]})
      if user is None:
        return jsonify({'message' : "User doesn't exist"}), 404
    except:
      return jsonify({'message' : 'Token is invalid'}), 404
    return func(*args, **kwargs)
  return jwt_required_wrapper

#ADMIN
def admin_required(func):
  @wraps(func)
  def admin_required_wrapper(*args, **kwargs):
    token = None
    if 'x-access-token' in request.headers:
      token = request.headers['x-access-token']
    if not token:
      return jsonify({'message' : 'Token is missing'}), 401
    try:
      data = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
      user = users.find_one({"username": data["username"]})
      if user is None:
        return jsonify({'message' : "User doesn't exist"}), 404
      if user["is_admin"] == "false":
        return jsonify({'message' : "User does not have permission"}), 403
    except:
      return jsonify({'message' : 'Token is invalid'}), 404
    return func(*args, **kwargs)
  return admin_required_wrapper

# **GET Routes**
#/products
@app.route("/api/v1/products/", methods = ["GET"])
def get_products():
  page_num, page_size = 1, 10

  if request.args.get("page_num"):
    page_num = int(request.args.get('page_num'))
  if request.args.get("page_size"):
    page_size = int(request.args.get('page_size'))

  page_start = ((page_size * (page_num -1)))
  page_end = (page_start + page_size)

  response = []

  for product in products.find({},{"reviews":0}).skip(page_start).limit(page_end):
    product["_id"] = str(product["_id"])

    response.append(product)

  return make_response(jsonify( response ), 200)

#/products/:id
@app.route("/api/v1/products/<string:id>/", methods = ["GET"])
def get_product(id):

  page_num, page_size = 1, 10

  if request.args.get("page_num"):
    page_num = int(request.args.get('page_num'))
  if request.args.get("page_size"):
    page_size = int(request.args.get('page_size'))

  page_start = ((page_size * (page_num -1)))
  page_end = (page_start + page_size)

  if ObjectId.is_valid(id):

    product = products.find_one({"_id": ObjectId(id)})

    if product != None:
      product["_id"] = str(product["_id"])
      if "reviews" in product:
        product["reviews"] = format_reviews(product, page_start, page_end)

      return make_response(jsonify ( product ), 200)
    return make_response(jsonify({"error":"Product does not exist"}), 404)
  return make_response(jsonify({"error":"Invalid Id"}), 404)

#/products/:id/reviews
@app.route("/api/v1/products/<string:id>/reviews/", methods = ["GET"])
def get_product_reviews(id):
  page_num, page_size = 1, 10

  if request.args.get("page_num"):
    page_num = int(request.args.get('page_num'))
  if request.args.get("page_size"):
    page_size = int(request.args.get('page_size'))

  page_start = ((page_size * (page_num -1)))
  page_end = (page_start + page_size)

  if ObjectId.is_valid(id):

    product = products.find_one({"_id": ObjectId(id)})

    if product != None:
      product["_id"] = str(product["_id"])
      if "reviews" in product:
        response = format_reviews(product, page_start, page_end)

        return make_response(jsonify ( response ), 200)
      return make_response(jsonify({"message": "product has no reviews"}), 200)
    return make_response(jsonify({"error":"Product does not exist"}), 404)
  return make_response(jsonify({"error":"Invalid Id"}), 404)

#/products/:id/reviews/:id
@app.route("/api/v1/products/<string:product_id>/reviews/<string:review_id>/", methods = ["GET"])
def get_product_review(product_id, review_id):
  if ObjectId.is_valid(product_id) and ObjectId.is_valid(review_id):
    reviews = products.find_one({"reviews._id": ObjectId(review_id)},{"_id": 0, "reviews.$" : 1})
    print(reviews)
    if reviews != None:
      reviews["reviews"][0]["_id"] = str(reviews["reviews"][0]["_id"])
      return make_response(jsonify ( reviews["reviews"][0] ), 200)
    return make_response(jsonify({"error":"Review does not exist"}), 404)
  return make_response(jsonify({"error":"One or more Ids invalid"}), 404)

#/users/
@app.route("/api/v1/users/", methods = ["GET"])
@admin_required
def get_users():
  page_num, page_size = 1, 10

  if request.args.get("page_num"):
    page_num = int(request.args.get('page_num'))
  if request.args.get("page_size"):
    page_size = int(request.args.get('page_size'))

  page_start = ((page_size * (page_num -1)))
  page_end = (page_start + page_size)

  response = []

  for user in users.find({},{"password":0}).skip(page_start).limit(page_end):
    user["_id"] = str(user["_id"])

    response.append(user)

  return make_response(jsonify( response ), 200)

#/users/:id
@app.route("/api/v1/users/<string:id>/", methods = ["GET"])
@jwt_required
def get_user(id):
  if ObjectId.is_valid(id):

    user = users.find_one({"_id": ObjectId(id)},{"password":0})

    if user != None:
      user["_id"] = str(user["_id"])

      return make_response(jsonify ( user ), 200)
    return make_response(jsonify({"error":"User does not exist"}), 404)
  return make_response(jsonify({"error":"Invalid Id"}), 404)

@app.route("/api/v1/checkAdmin/", methods = ["GET"])
@admin_required
def check_admin():
  return make_response(jsonify({"success": "user is admin"}), 200)

@app.route("/api/v1/checkLogged/", methods = ["GET"])
@jwt_required
def check_logged():
  return make_response(jsonify({"success": "user is logged in"}), 200)

@app.route("/api/v1/checkUser/<string:id>/", methods = ["GET"])
@jwt_required
def check_user(id):
  jwt = request.headers['x-access-token']
  user = get_logged_user(jwt)
  if (str(user['_id']) == id):
    return make_response(jsonify({"success": "belongs to user"}), 200)
  return make_response(jsonify({"error": "does not belong to user"}), 403)

# *POST Routes*
#/products
@app.route("/api/v1/products/", methods = ["POST"])
@admin_required
def create_product():
  if "title" in request.form and "description" in request.form and "price" in request.form and "type" in request.form:
    if is_string(request.form["title"]) and is_string(request.form["description"]) and is_float(request.form["price"]) and is_string(request.form["type"]):
      new_product =  {"title" : request.form["title"],
                      "description" : request.form["description"],
                      "price" : round(float(request.form["price"]), 2),
                      "type" : request.form["type"]}
      new_product = products.insert_one(new_product)
      new_product_link = "/products/" + str(new_product.inserted_id)
      return make_response(jsonify({"url" : new_product_link}), 201)
    return make_response(jsonify({"error": "invalid form data"}), 400)
  return make_response(jsonify({"error" : "data missing"}), 400)

#/products/:id/reviews
@app.route("/api/v1/products/<string:id>/reviews/", methods = ["POST"])
@jwt_required
def create_product_review(id):
  jwt = request.headers['x-access-token']
  user = get_logged_user(jwt)
  if "comment" in request.form and "rating" in request.form:
    if is_string(request.form["comment"]) and is_rating(request.form["rating"]):
      new_review = {"_id" : ObjectId(),
                    "user" : str(user["_id"]),
                    "comment" : request.form["comment"],
                    "rating" : int(request.form["rating"]),
                    "review_posted" : datetime.datetime.utcnow()}
      products.update_one( {"_id" : ObjectId(id)}, {"$push": { "reviews" : new_review}})
      new_review_link = "/products/" + id + "/reviews/" + str(new_review['_id'])
      return make_response(jsonify({"url" : new_review_link}), 201)
    return make_response(jsonify({"error": "invalid form data"}), 400)
  return make_response(jsonify({"error" : "data missing"}), 400)

#/users
@app.route("/api/v1/users/", methods = ["POST"])
@admin_required
def create_user():
  if "username" in request.form and "email" in request.form and "password" in request.form and "password_confirm" in request.form and "is_admin" in request.form:
    if is_string(request.form["username"]) and is_string(request.form["email"]) and is_string(request.form["password"]) and is_string(request.form["password_confirm"]) and is_string(request.form["is_admin"]):
      if request.form["password"] == request.form["password_confirm"]:
        if request.form["is_admin"] == "true" or request.form["is_admin"] == "false":
          password = bytes(request.form["password"], 'utf-8')
          encrypted_password = bcrypt.hashpw(password, bcrypt.gensalt())

          new_user = {"username" : request.form["username"],
                      "email" : request.form["email"],
                      "password" : encrypted_password,
                      "user_created" : datetime.datetime.utcnow(),
                      "is_admin" : request.form["is_admin"]}
          new_user = users.insert_one(new_user)
          new_user_link = "/users/" + str(new_user.inserted_id)
          return make_response(jsonify({"url" : new_user_link}), 201)
        return make_response(jsonify({"error": '"is_admin" must be true or false'}), 400)
      return make_response(jsonify({"error": "passwords do not match"}), 400)
    return make_response(jsonify({"error": "invalid form data"}), 400)
  return make_response(jsonify({"error" : "data missing"}), 400)

#/register
@app.route("/api/v1/register/", methods = ["POST"])
def register_user():
  if "username" in request.form and "email" in request.form and "password" in request.form and "password_confirm" in request.form:
    if is_string(request.form["username"]) and is_string(request.form["email"]) and is_string(request.form["password"]) and is_string(request.form["password_confirm"]):
      if request.form["password"] == request.form["password_confirm"]:
        password = bytes(request.form["password"], 'utf-8')
        encrypted_password = bcrypt.hashpw(password, bcrypt.gensalt())

        new_user = {"username" : request.form["username"],
                    "email" : request.form["email"],
                    "password" : encrypted_password,
                    "user_created" : datetime.datetime.utcnow(),
                    "is_admin" : "false"}
        new_user = users.insert_one(new_user)

        token = jwt.encode({'username' : request.form['username'],
                            'email' : request.form['email'],
                            'is_admin' : "false", 
                            'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
                            }, app.config['SECRET_KEY'], algorithm="HS256")
        return make_response(jsonify({'token_encoded' : token}), 201)
      return make_response(jsonify({"error": "passwords do not match"}), 400)
    return make_response(jsonify({"error": "invalid form data"}), 400)
  return make_response(jsonify({"error" : "data missing"}), 400)

#/login
@app.route("/api/v1/login/", methods = ["POST"])
def create_login():
  auth = request.authorization
  if auth:
    user = users.find_one({"username":auth.username})
    if user is not None:
      if bcrypt.checkpw(bytes(auth.password, 'UTF-8'), user["password"]):
        token = jwt.encode({'username' : user['username'],
                            'email' : user['email'],
                            'is_admin' : user['is_admin'], 
                            'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
                            }, app.config['SECRET_KEY'], algorithm="HS256")
        return make_response(jsonify({'token_encoded' : token}), 201)
      return make_response(jsonify({'error':'Bad password'}), 403)
    return make_response(jsonify({'error':'User does not exist'}), 404)
  return make_response(jsonify({'error':'Authentication required'}), 401)


#**PUT Routes**
#/products/:id
@app.route("/api/v1/products/<string:id>/", methods = ["PUT"])
@admin_required
def update_product(id):
  if "title" in request.form and "description" in request.form and "price" in request.form and "type" in request.form:
    if is_string(request.form["title"]) and is_string(request.form["description"]) and is_float(request.form["price"]) and is_string(request.form["type"]):
      products.update_one( {"_id" : ObjectId(id)}, {"$set": {"title" : request.form["title"],
                      "description" : request.form["description"],
                      "price" : round(float(request.form["price"]), 2),
                      "type" : request.form["type"]}})
      updated_product_link = "/products/" + id
      return make_response(jsonify({"url" : updated_product_link}), 201)
    return make_response(jsonify({"error": "invalid form data"}), 400)
  return make_response(jsonify({"error" : "data missing"}), 400)

#/products/:id/reviews/:id
@app.route("/api/v1/products/<string:product_id>/reviews/<string:review_id>/", methods = ["PUT"])
@jwt_required
def update_product_review(product_id, review_id):
  jwt = request.headers['x-access-token']
  user = get_logged_user(jwt)
  if ObjectId.is_valid(product_id) and ObjectId.is_valid(review_id):
    reviews = products.find_one({"reviews._id": ObjectId(review_id)},{"_id": 0, "reviews.$" : 1})
    if reviews != None:
      review = reviews["reviews"][0]
      review["_id"] = str(review["_id"])
      if review["user"] == str(user["_id"]):
        if "comment" in request.form and "rating" in request.form:
          if is_string(request.form["comment"]) and is_rating(request.form["rating"]):
            edited_review = {"reviews.$.comment" : request.form["comment"],
                                "reviews.$.rating" : int(request.form["rating"])}
            products.update_one( {"reviews._id" : ObjectId(review_id)}, {"$set": edited_review})
            edited_review_link = "/products/" + product_id + "/reviews/" + review_id
            return make_response(jsonify({"url" : edited_review_link}), 201)
          return make_response(jsonify({"error": "invalid form data"}), 400)
        return make_response(jsonify({"error" : "data missing"}), 400)
      return make_response(jsonify({"error" : "you do not have permission to edit this review"}), 403)
    return make_response(jsonify({"error" : "review does not exist"}), 404)
  return make_response(jsonify({"error":"One or more Ids invalid"}), 404)

#/user/:id
@app.route("/api/v1/users/<string:id>/", methods = ["PUT"])
@jwt_required
def update_user(id):
  jwt = request.headers['x-access-token']
  user = get_logged_user(jwt)
  if user != None:
    if str(user["_id"]) == id or user["is_admin"] == "true":
      print("passed user check")
      if "username" in request.form and "email" in request.form and "password" in request.form:
        if is_string(request.form["username"]) and is_string(request.form["email"]) and is_string(request.form["password"]):
          password = bytes(request.form["password"], 'utf-8')
          encrypted_password = bcrypt.hashpw(password, bcrypt.gensalt())
          print("about to udate")
          users.update_one( {"_id" : ObjectId(id)}, {"$set": {"username" : request.form["username"],
                      "email" : request.form["email"],
                      "password" : encrypted_password}})
          updated_user_link = "/users/" + id
          return make_response(jsonify({"url" : updated_user_link}), 201)
        return make_response(jsonify({"error": "invalid form data"}), 400)
      return make_response(jsonify({"error" : "data missing"}), 400)
    return make_response(jsonify({"error": "you do not have permission to edit this user"}), 403)
  return make_response(jsonify({"error": "user could not be found"}), 404)


#**DELETE Routes**
#/products/:id
@app.route("/api/v1/products/<string:id>/", methods = ["DELETE"])
@admin_required
def delete_product(id):
  result = products.delete_one({"_id" : ObjectId(id)})
  if result.deleted_count == 1:
    return make_response(jsonify({"success": "product was successfully deleted"}), 200)
  return make_response(jsonify({"error": "product could not be deleted"}), 400)

#/products/:id/reviews/:id
@app.route("/api/v1/products/<string:product_id>/reviews/<string:review_id>/", methods = ["DELETE"])
@jwt_required
def delete_product_review(product_id, review_id):
  jwt = request.headers['x-access-token']
  user = get_logged_user(jwt)
  if ObjectId.is_valid(product_id) and ObjectId.is_valid(review_id):
    reviews = products.find_one({"reviews._id": ObjectId(review_id)},{"_id": 0, "reviews.$" : 1})
    if reviews != None:
      review = reviews["reviews"][0]
      if str(review["user"]) == str(user["_id"]):
        products.delete_one({"reviews._id": ObjectId(review_id)})
        return make_response(jsonify({"success": "review was successfully deleted"}), 200)
      return make_response(jsonify({"error": "you do not have permission to delete this review"}), 403)
    return make_response(jsonify({"error": "review not found"}), 404)
  return make_response(jsonify({"error": "one or more ids invalid"}), 400)

#/user/:id
@app.route("/api/v1/users/<string:id>/", methods = ["DELETE"])
@jwt_required
def delete_user(id):
  jwt = request.headers['x-access-token']
  user = get_logged_user(jwt)
  if user != None:
    if str(user["_id"]) == id or user["is_admin"] == "true":
      result = users.delete_one({"_id" : ObjectId(id)})
      if result.deleted_count == 1:
        return make_response(jsonify({"success": "user was successfully deleted"}), 200)
      return make_response(jsonify({"error": "user could not be deleted"}), 400)
    return make_response(jsonify({"error": "you do not have permission to delete this user"}), 403)
  return make_response(jsonify({"error": "user could not be found"}), 404)

def format_reviews(product, page_start, page_end):
  response = [] 
  x = 0
  print(product)

  for review in product["reviews"]:
    
    if x >= page_start and x < page_end:
      if "_id" in review:
        review["_id"] = str(review["_id"]) 
        response.append(review)
    x = x +1
    

  return response

def get_logged_user(token):
  data = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
  user = users.find_one({"username": data["username"]})
  return user

def is_string(string):
  string = string.strip()
  try:
    str(string)
    if string == "":
      return False
    return True
  except ValueError:
    return False

def is_rating(num):
  try:
    int(num)
    if int(num) > 0 and int(num) < 6:
      return True
    return True
  except ValueError:
    return False

def is_float(num):
  try:
    float(num)
    return True
  except ValueError:
    return False

if __name__ == "__main__":
  app.run(debug = True, host="0.0.0.0")